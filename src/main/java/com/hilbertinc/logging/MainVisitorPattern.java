package com.hilbertinc.logging;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.hilbertinc.base.HException;
import com.hilbertinc.xml.HDOMParser;
import com.hilbertinc.xml.HDumperVisitor;
import com.visitor.part2.html.HtmlDumperVisitor;
import com.visitor.part3.html.HTMLReflectiveDOMParser;
import com.visitor.part3.html.HTMLReflectiveVisitor;

/**
 * This is a disposable class that I am using for unit testing the framework
 */
public class MainVisitorPattern {
    /**
     * UnitTest constructor comment.
     */
    public MainVisitorPattern() {
        super();
    }

    /**
     * Starts the application.
     * 
     * @param args
     *            an array of command-line arguments
     * @throws HException
     * @throws FileNotFoundException
     */
    public static void main(java.lang.String[] args) throws Exception {

        String filename = StringUtils.EMPTY;
        if (0 == args.length) {
            System.err.println("Syntax: java COM.hilbertinc.xml.UnitTest <xmldocument>");
            return;
        } else {
            filename = args[0];
        }

        System.out.println("Part 1 Output !!!");
        xmlVisitor(filename);
        System.out.println("/n/n/nPart 1 Output !!!");
        htmlVisitor(filename);
        System.out.println("/n/n/nPart 1 Output !!!");
        htmlReflectiveVisitor(filename);
    }

    private static void htmlReflectiveVisitor(String filename) throws HException, IOException {
        HTMLReflectiveDOMParser domParser = getReflectiveParser(filename);
        HTMLReflectiveVisitor reflectiveVisitor = new HTMLReflectiveVisitor();
        domParser.traverse(reflectiveVisitor);
        reflectiveVisitor.flush();
        System.out.println("Done with reflection. Reflection is awesome");
    }

    private static void htmlVisitor(String filename) throws FileNotFoundException, HException {
        try {
            HDOMParser parser = getParser(filename);
            HtmlDumperVisitor htmlDumper = new HtmlDumperVisitor();
            parser.traverse(htmlDumper);
            htmlDumper.flush();
            System.out.println("Done.");
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private static void xmlVisitor(String filename) {
        try {
            HDOMParser parser = getParser(filename);
            HDumperVisitor dumper = new HDumperVisitor();
            parser.traverse(dumper);
            dumper.flush();
            System.out.println("Done.");
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
    
    private static HTMLReflectiveDOMParser getReflectiveParser(String filename) throws HException {
        System.out.println("Parsing...");
        HTMLReflectiveDOMParser parser = new HTMLReflectiveDOMParser();
        parser.parse(filename);
        if (null == parser.getDocument()) {
            System.out.println("Document was not successfully parsed");
            System.out
                    .println("If there are no error messages, it is probably an inability to find the DTD");
            System.exit(1);
        }
        System.out.println("Dumping contents of parsed document completed");
        return parser;
    }

    private static HDOMParser getParser(String filename) throws HException {
        System.out.println("Parsing...");
        HDOMParser parser = new HDOMParser();
        parser.parse(filename);
        if (null == parser.getDocument()) {
            System.out.println("Document was not successfully parsed");
            System.out
                    .println("If there are no error messages, it is probably an inability to find the DTD");
            System.exit(1);
        }
        System.out.println("Dumping contents of parsed document completed");
        return parser;
    }
}