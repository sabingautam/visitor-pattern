package com.visitor.part3.html;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.hilbertinc.xml.AbstractReflectiveVisitor;
import com.hilbertinc.xml.HDOMUtil;

public class HTMLReflectiveVisitor extends AbstractReflectiveVisitor {

    private Writer writer = null;
    private static final int INDENT_VALUE = 4;
    private int indent = 0;
    private int htmlHeaderNumber = 0;
    private Set<String> visitedElements = new HashSet<String>();

    public void flush() throws IOException {
        getWriter().flush();
    }

    private String getIndent() {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < indent; ++i)
            buffer.append(' ');
        return buffer.toString();
    }

    private void setHtmlHeaderNumber() {
        int currentIndentValue = indent;
        int headerNumber = currentIndentValue / INDENT_VALUE;
        if (headerNumber > 1) {
            htmlHeaderNumber = headerNumber - 1;
        }
    }

    private Writer getWriter() {
        if (null == writer) {
            writer = new PrintWriter(System.out);
        }
        return writer;
    }

    private void indentLess() {
        indent -= INDENT_VALUE;
        setHtmlHeaderNumber();
    }

    private void indentMore() {
        indent += INDENT_VALUE;
        setHtmlHeaderNumber();
    }

    private boolean isVisited(String tagName) {
        if (visitedElements.contains(tagName)) {
            visitedElements.remove(tagName);
            return true;
        }
        visitedElements.add(tagName);
        return false;
    }

    public void visitDocumentType(DocumentType document) throws IOException {
        Writer xml = getWriter();
        boolean isVisited = isVisited(document.getName());

        if (!isVisited) {
            processDocumentProlog(document.getName(), xml);
        }
    }

    private void processDocumentProlog(String nodeName, Writer xml)
            throws IOException {
        processHtmlPageProlog(xml);
        processElementHtmlTagProlog(xml);
        xml.write("&lt!DOCTYPE ");
        xml.write(nodeName);
        xml.write(" &gt ");
        processElementHtmlTagEpilog(xml);
    }

    public void visitElement(Element element) throws Exception {
        boolean isVisited = isVisited(element.getTagName());
        Writer xml = getWriter();
        String nodeName = element.getTagName();
        if (!isVisited) {
            processElementProlog(nodeName, xml);
        } else {
            processElementEpilog(nodeName, xml);
        }
    }

    private void processElementProlog(String nodeName, Writer xml)
            throws IOException {
        xml.write(getIndent());
        processElementHtmlTagProlog(xml);
        xml.write("&lt");
        xml.write(nodeName);
        xml.write("&gt");
        processElementHtmlTagEpilog(xml);
        indentMore();
    }

    private void processElementEpilog(String nodeName, Writer xml)
            throws Exception {
        indentLess();
        xml.write(getIndent());
        processElementHtmlTagProlog(xml);
        xml.write("&lt/");
        xml.write(nodeName);
        xml.write("&gt");
        processElementHtmlTagEpilog(xml);
    }

    public void visitText(Text text) throws IOException {
        String sanitizedText = HDOMUtil.trim(text);
        boolean isVisited = isVisited(sanitizedText);
        if (!isVisited) {
            Writer xml = getWriter();
            xml.write(getIndent());
            xml.write(sanitizedText);
            xml.write("\n");
        }
    }

    public void visitDocument(Document document) throws Exception {
        getWriter().write("Document prolog\n");
        getWriter().write("Document epilog\n");
        return;
    }

    public void visitObject(Object o) {
        System.out.println("Well, i wanted to print something, but i don't"
                + " know what you want me to do with this type of object "
                + o.getClass().getName());
        ;
    }

    protected void processHtmlPageProlog(Writer xml) throws IOException {
        xml.write("<!DOCTYPE html >\n");
        xml.write("<html>\n");
        indentMore();
        xml.write(getIndent());
        xml.write("<body>\n");
        indentMore();
        xml.write(getIndent());
    }

    public void processHtmlPageEpilog() throws IOException {
        Writer xml = getWriter();
        indentLess();
        xml.write(getIndent());
        xml.write("</body>\n");
        indentLess();
        xml.write("</html>\n");
    }

    protected void processElementHtmlTagProlog(Writer xml) throws IOException {
        xml.write("<h" + htmlHeaderNumber + ">");
    }

    protected void processElementHtmlTagEpilog(Writer xml) throws IOException {
        xml.write("</h" + htmlHeaderNumber + ">\n");
    }

    public void visit(Object o) {
        Method visitMethod = getMethod(o.getClass());

        // Lets see if we can invoke that method now.
        try {
            // Try to invoke the method
            visitMethod.invoke(this, new Object[] { o });
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            // System.err.println("Illegal Access Exception");
            visitObject(o);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            // System.err.println("Illegal Argument Exception");
            visitObject(o);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            // System.err.println("Invocation Target Exception");
            visitObject(o);
        }
    }
}
