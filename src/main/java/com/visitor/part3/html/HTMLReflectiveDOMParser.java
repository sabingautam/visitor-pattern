package com.visitor.part3.html;

import java.io.IOException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hilbertinc.xml.HDOMParser;
import com.hilbertinc.xml.Visitor;

public class HTMLReflectiveDOMParser extends HDOMParser {

    protected void processNode(NodeList nodes, Visitor visitor) {

        int count = nodes.getLength();

        for (int i = 0; i < count; i++) {
            Node node = nodes.item(i);
            visitor.visit(node);
            if (node.hasChildNodes()) {
                processNode(node.getChildNodes(), visitor);
            }
            visitor.visit(node);
        }
    }

    public void traverse(Visitor visitor) throws IOException {
        if (null == getDocument()) {
            return; // Nothing to traverse
        }

        if (null == visitor) {
            return; // Nothing to do once we get to a node, so we might as well
                    // leave now
        }
        NodeList children = getDocument().getChildNodes();
        processNode(children, visitor);
        if (visitor instanceof HTMLReflectiveVisitor) {
            ((HTMLReflectiveVisitor) visitor).processHtmlPageEpilog();
        }
    }
}
