package com.visitor.part2.html;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

import com.hilbertinc.xml.HDOMUtil;
import com.hilbertinc.xml.HDOMVisitor;


public class HtmlDumperVisitor implements HDOMVisitor{

    private Writer writer = null;
    private static final int INDENT_VALUE = 4;
    private int indent = 0;
    private int htmlHeaderNumber = 0;

    public HtmlDumperVisitor() {
        super();
    }

    public boolean continueTraversal() {
        return true;
    }

    public void flush() throws IOException {
        getWriter().flush();
        return;
    }

    protected String getIndent() {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < indent; ++i)
            buffer.append(' ');
        return buffer.toString();
    }

    protected void setHtmlHeaderNumber() {
        int currentIndentValue = indent;
        int headerNumber = currentIndentValue/INDENT_VALUE;
        if (headerNumber > 1) {
            htmlHeaderNumber = headerNumber - 1;
        }
    }

    public Writer getWriter() {
        if (null == writer)
            writer = new PrintWriter(System.out);
        return writer;
    }

    protected void indentLess() {
        indent -= INDENT_VALUE;
        setHtmlHeaderNumber();
    }

    protected void indentMore() {
        indent += INDENT_VALUE;
        setHtmlHeaderNumber();
    }

    public void processDocumentEpilog(org.w3c.dom.Document document)
            throws Exception {
        getWriter().write("Document epilog\n");
        return;
    }

    public void processDocumentProlog(org.w3c.dom.Document document)
            throws Exception {
        getWriter().write("Document prolog\n");
        return;
    }

    public void processDocumentType(DocumentType dtd) throws Exception {
        Writer xml = getWriter();
        processHtmlPageProlog(xml);
        processElementHtmlTagProlog(xml);
        xml.write("&lt!DOCTYPE ");
        xml.write(dtd.getName());
        xml.write(" &gt ");
        processElementHtmlTagEpilog(xml);
        return;
    }

    protected void processHtmlPageProlog(Writer xml) throws IOException {
        xml.write("<!DOCTYPE html >\n");
        xml.write("<html>\n");
        indentMore();
        xml.write(getIndent());
        xml.write("<body>\n");
        indentMore();
        xml.write(getIndent());
    }

    public void processElementEpilog(Element node) throws Exception {
        Writer xml = getWriter();
        indentLess();
        xml.write(getIndent());
        processElementHtmlTagProlog(xml);
        xml.write("&lt/");
        xml.write(node.getNodeName());
        xml.write("&gt");
        processElementHtmlTagEpilog(xml);
        return;
    }

    protected void processElementHtmlTagEpilog(Writer xml) throws IOException {
        xml.write("</h" + htmlHeaderNumber + ">\n");
    }

    public void processElementProlog(Element node) throws Exception {
        Writer xml = getWriter();
        xml.write(getIndent());
        processElementHtmlTagProlog(xml);
        xml.write("&lt");
        xml.write(node.getTagName());
        xml.write("&gt");
        processElementHtmlTagEpilog(xml);
        indentMore();
        return;
    }

    protected void processElementHtmlTagProlog(Writer xml) throws IOException {
        xml.write("<h" + htmlHeaderNumber + ">");
    }

    public void processText(org.w3c.dom.Text text) throws Exception {
        Writer xml = getWriter();
        xml.write(getIndent());
        xml.write(HDOMUtil.trim(text));
        xml.write("\n");
        return;
    }

    public void processHtmlPageEpilog() throws IOException {
        Writer xml = getWriter();
        indentLess();
        xml.write(getIndent());
        xml.write("</body>\n");
        indentLess();
        xml.write("</html>\n");
    }

    public void setWriter(Writer xmlWriter) {
        writer = xmlWriter;
        return;
    }
}
